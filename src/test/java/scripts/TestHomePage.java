package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.AutomationExerciseBasePage;
import utilities.Driver;

import java.util.List;

public class TestHomePage {


    WebDriver driver;
    AutomationExerciseBasePage automationExerciseBasePage;


    @BeforeMethod
    public void setup() {

        driver = Driver.getDriver();
        automationExerciseBasePage = new AutomationExerciseBasePage();
        driver.get("https://automationexercise.com/");
    }

    @AfterMethod
    public void teardown() {
        //We will quit driver and do other proper clean ups
        Driver.quitDriver();
    }




}
