package scripts;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AutomationExerciseBasePage;

public class TestCase1 extends TestHomePage {

    //Test Case 1: Validate Automation Exercise site logo
    //Given user navigates to “https://automationexercise.com/”
    //Then user should see logo on top-left

    @Test
    public void validateAutomationLogo() {
        Assert.assertTrue(AutomationExerciseBasePage.logo.isDisplayed());
    }



}
