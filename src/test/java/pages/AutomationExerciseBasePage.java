package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.Driver;

import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class AutomationExerciseBasePage {



    public AutomationExerciseBasePage() {
        PageFactory.initElements(Driver.getDriver(), this);
    }


    @FindBy(className = "logo")
    public static WebElement logo;

    @FindBy(className = "navbar-nav")
    public static List<WebElement> navBar;

    @FindBy(css = "#dropdown-menu a")
    public List<WebElement> headerDropdownOptions;



}
